### Matemática Aplicada à Biologia (UFC-CB689)

Disciplina ministrada para o curso de Biologia da Universidade Federal do Ceará (UFC),
durante o semestre 2016.2.

#### Sobre o repositório

Esse repositório contém o material didático (referências bibliográficas,
listas de exercícios e avaliações) utilizadas durante a disciplina.

#### Ementa

![ementa-UFC-CB689](./ufc-cb689-ementa.jpg)